function oddEvenChecker(number){
	if(typeof number === "number"){
		if (number % 2 == 0){
			console.log("The number is even.")
		} else {
			console.log("The number is odd.")
		}
	} else {
		alert("Invalid Input")
	}
};

oddEvenChecker(33);

function budgetChecker(budget){
	if (typeof budget === "number"){
		if (budget > 40000){
			console.log("You are over the budget.")
		} else if (budget < 40000){
			console.log("You have resources left.")
		}
	} else {
		alert("Invalid Input")
	}
};

budgetChecker(20);